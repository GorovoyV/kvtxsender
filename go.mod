module txsend

go 1.15

require (
	github.com/certifi/gocertifi v0.0.0-20200922220541-2c3bb06c6054 // indirect
	github.com/evalphobia/logrus_sentry v0.8.2 // indirect
	github.com/fatih/structs v1.1.0 // indirect
	github.com/getsentry/raven-go v0.2.0 // indirect
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible // indirect
	github.com/nullstyle/go-xdr v0.0.0-20180726165426-f4c839f75077 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.7.0 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/viper v1.7.1 // indirect
	gitlab.com/distributed_lab/figure v2.1.0+incompatible // indirect
	gitlab.com/distributed_lab/json-api-connector v0.2.3 // indirect
	gitlab.com/distributed_lab/kit v1.8.3 // indirect
	gitlab.com/distributed_lab/logan v3.8.0+incompatible // indirect
	gitlab.com/tokend/connectors v0.1.3
	gitlab.com/tokend/go v3.13.1+incompatible
	gitlab.com/tokend/keypair v0.0.0-20190412110653-b9d7e0c8b312
	gitlab.com/tokend/regources v4.9.1+incompatible // indirect
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
)
