package main

import (
	"context"
	"gitlab.com/tokend/connectors/signed"
	"gitlab.com/tokend/connectors/submit"
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/keypair"
	"net/http"
	"net/url"
)

func main() {

	//var kp keypair.Full
	//var err error
	//kp.Address() = "GBA4EX43M25UPV4WIE6RRMQOFTWXZZRIPFAI5VPY6Z2ZVVXVWZ6NEOOB"
	kp, err := keypair.ParseSeed("SAMJKTZVW5UOHCDK5INYJNORF2HRKYI72M5XSZCBYAHQHR34FFR4Z6G4")
	if err != nil {
		println(err.Error())
		return
	}
	println(kp)
	println(kp.Address())
	var op xdrbuild.Operation
	op = xdrbuild.PutKeyValue{
		Key:   "aaq",
		Value: uint32(38),
	}
//	opKV,err:=op.XDR()

	if err != nil{
		println(err.Error())
	}
	endp,err:= url.Parse("http://localhost:8092")
	if err != nil{
		println(err.Error())
	}
	horizonClient := signed.NewClient(http.DefaultClient, endp )
	submitter := submit.New(horizonClient)
	tBuilder,err := submitter.TXBuilder()
	if err != nil{
		println(err.Error())
	}
	builder, err := tBuilder.Transaction(kp).Op(op).Sign(kp).Marshal()
	if err != nil{
		println(err.Error())
	}
	result,err := submitter.Submit(context.TODO(),builder,false)
	if err != nil{
		println(err.Error())
		return
	}

	println(result)
}